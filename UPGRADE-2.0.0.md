# Upgrade from 1.1 to 2.0

- JIT Image has been removed (due to lack of support for Laravel 5) and replaced
  with Intervention. To utilise intervention in the same way as jitimage:
	- First publish the imagecache config using the Artisan command `php artisan vendor:publish --provider=Intervention\Image\ImageServiceProviderLaravel5`
	- Define the route as "images"
	- Translate your "recipies" into templates, using the expressive API
	  (http://image.intervention.io/use/basics#editing)

- The Larasoft\MediaLibrary namespace has been consolidated with Larasoft\Admin.
  There have also been a number of directories inside Larasoft\Admin reorganised.
  The full mapping of old classes to new is as follows:
	- Larasoft\MediaLibrary\MediaLibraryServiceProvider => Larasoft\Admin\Providers\MediaLibraryServiceProvider
	- Larasoft\MediaLibrary\Controllers\MediaLibraryAdminController => Larasoft\Admin\Http\Controllers\MediaLibraryAdminController
	- Larasoft\MediaLibrary\Decorators\MediaAdminDecorator => Larasoft\Admin\Decorators\MediaAdminDecorator
	- Larasoft\MediaLibrary\Fields\MediaBrowser => Larasoft\Admin\Fields\MediaBrowser
	- Larasoft\MediaLibrary\Fields\MediaField => Larasoft\Admin\Fields\MediaField
	- Larasoft\MediaLibrary\Models\Media => Larasoft\Admin\Models\Media
	- Larasoft\MediaLibrary\Models\MediableTrait => Larasoft\Admin\Traits\MediableTrait
	- Larasoft\MediaLibrary\Validators\MediaValidator => Larasoft\Admin\Services\Validators\MediaValidator
	- Larasoft\Admin\AdminServiceProvider => Larasoft\Admin\Providers\AdminServiceProvider
	- Larasoft\Admin\Components\Menu => Larasoft\Admin\Base\Components\Menu
	- Larasoft\Admin\Composers\Nav => Larasoft\Admin\Base\Composers\Nav
	- Larasoft\Admin\Decorators\ModelAdminDecorator => Larasoft\Admin\Base\ModelAdminDecorator
	- Larasoft\Admin\Decorators\MediaAdminDecorator => Larasoft\Admin\Media\MediaAdminDecorator
	- Larasoft\Admin\Decorators\UserAdminDecorator => Larasoft\Admin\Users\UserAdminDecorator
	- Larasoft\Admin\Models\Base => Larasoft\Admin\Base\Model
	- Larasoft\Admin\Models\BaseInterface => Larasoft\Admin\Base\ModelInterface
	- Larasoft\Admin\Models\Media => Larasoft\Admin\Media\Media
	- Larasoft\Admin\Models\Sortable => Larasoft\Admin\Base\Sortable
	- Larasoft\Admin\Models\User => Larasoft\Admin\Users\User
	- Larasoft\Admin\Traits\DynamicSlugTrait => Larasoft\Admin\Base\DynamicSlugTrait
	- Larasoft\Admin\Traits\MediableTrait => Larasoft\Admin\Media\MediaTrait
	- Larasoft\Admin\Traits\SanitisesInputTrait => Larasoft\Admin\Base\SanitisesInputTrait

- Several classes in the Admin namespace have also been moved. The full mapping is as follows:
	- Larasoft\Admin\AdminServiceProvider => Larasoft\Admin\Providers\AdminServiceProvider
	- Larasoft\Admin\Controllers\ModelAdminController => Larasoft\Admin\Http\Controllers\ModelAdminController

- Pages have been removed from the Admin. This includes the following classes:
  Decorators\PageAdminDecorator, Controllers\PageAdminController,
  Controllers\PageController, Models\Page, Services\Validators\PageValidator and
  Subscribers\PageEventHandler. Please use an alternative implementation for
  handling pages.

- SearchListingFilter now accepts either an array of attributes OR a callback as
  its second parameter, not both.

- Field, and subclasses thereof, now must take an $errors argument, which is
  passed in from the view that renders it.

- ModelAdminDecorator::getModel has been removed. Do not rely on outside classes
  having knowledge of the decorator's underlying model.

- Services\Validator::passesEdit() and Services\Validator::$editRules have been
  removed. Use passesUpdate() and $updateRules instead.

- Reports\Report::overrideView() has been removed. Pass custom view in as second
  argument to constructor.

- Overview views must be passed the following variables: $editAction,
  $createAction and $destroyAction. These are passed in by default in the index
  method of ModelAdminController. If you are overriding the index method in your
  controller, ensure you pass `$this->getReportParams()` into the `render`
  method of the report.

- The `findInstanceOrFail` method has been removed from `ModelAdminDecorator`.
  This behaviour is now standard and has been moved into the `findInstance`
  method. Calls to `findInstance` will now throw a `ModelNotFoundException` if
  an ID cannot be found for that model.

- Typehints to `Models\BaseInterface` on `updateSyncRelations` and
  `injectSyncRelations` methods on `Decorators\MediaAdminDecorator` have been
  removed. Remove this typehint on any subclasses that override either of these
  methods.

- Protected method `renderCreateFormFor` on `ModelAdminController` has been
  removed, in favour of the more generic `renderFormFor` method, which is called
  in both the `edit()` and `create()` methods. Update calls to this method in
  controllers with a call to `renderFormFor($instance, $this->createView, 'POST', 'store')`.
