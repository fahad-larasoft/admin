<?php

namespace Larasoft\Admin\Base;

use Larasoft\Admin\Services\Validators\EmptyValidator;
use Illuminate\Database\Eloquent\Model as Eloquent;

abstract class Model extends Eloquent implements ModelInterface
{
	use SanitisesInputTrait;

	/**
	 * @var array
	 */
	protected $nullable = [];

	/**
	 * Return an empty validator as a default implementation of method
	 *
	 * @return Larasoft\Admin\Services\Validators\EmptyValidator
	 */
	public function getValidator()
	{
		return new EmptyValidator;
	}
}
