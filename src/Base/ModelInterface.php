<?php

namespace Larasoft\Admin\Base;

interface ModelInterface
{
	/**
	 * Get the Validator used by this model.
	 *
	 * @return Larasoft\Admin\Services\Validators\Validator
	 */
	public function getValidator();

	/**
	 * Sanitise form input, ready for database insertion
	 *
	 * @param  array  $input
	 * @return array
	 */
	public function sanitiseInput($input);

}
