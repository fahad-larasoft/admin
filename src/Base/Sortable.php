<?php

namespace Larasoft\Admin\Base;

/**
 * @deprecated Use Larasoft\Admin\Base\Sorting\Sortable
 */
interface Sortable
{
	public function sortBy();
}
