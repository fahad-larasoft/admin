<?php namespace Larasoft\Admin\Fields;

use Collective\Html\FormFacade as Form;

class EmailField extends Field
{
	public function getInput()
	{
		return Form::email($this->get('name'), $this->get('value'), $this->attributes);
	}
}
