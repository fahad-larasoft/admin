<?php namespace Larasoft\Admin\Fields;

use Collective\Html\FormFacade as Form;
use InvalidArgumentException;

class SelectField extends Field
{
	public function getInput()
	{
		if (!isset($this->options)) {
			throw new InvalidArgumentException('You must define an "options" key mapping to an array');
		}

		$attributes = $this->attributes;
		unset($attributes['options']);

        if (isset($this->value) && !is_array($this->value) && !is_string($this->value) && $this->label != "Country"){
            $this->value = $this->value->toArray();
        }

		return Form::select($this->name, $this->options, $this->value, $attributes);
	}
}
