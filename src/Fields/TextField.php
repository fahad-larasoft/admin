<?php namespace Larasoft\Admin\Fields;

use Collective\Html\FormFacade as Form;

class TextField extends Field
{
	public function getInput()
	{
		return Form::text($this->get('name'), $this->get('value'), $this->attributes);
	}
}
