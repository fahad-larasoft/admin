<?php

namespace Larasoft\Admin\Http\Controllers;

use Larasoft\Admin\Permissions\PermissionAdminDecorator;
use Larasoft\Admin\Permissions\RestrictAllPermissionsTrait;

class PermissionAdminController extends ModelAdminController
{
	protected $useActions = true;

	use RestrictAllPermissionsTrait;

	public function __construct(PermissionAdminDecorator $decorator)
	{
		parent::__construct($decorator);
	}

	protected function getRestrictRule()
	{
		return 'manage_permissions';
	}
}
