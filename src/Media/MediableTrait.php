<?php

namespace Larasoft\Admin\Media;

trait MediableTrait
{
	public function media()
	{
		return Media::forModel($this);
	}
}
