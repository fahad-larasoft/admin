<?php

namespace Larasoft\Admin\Media;

use Larasoft\Admin\Base\Model;

class Tag extends Model
{
	protected $table = 'media_tags';

	protected $fillable = ['name'];

	public function media()
	{
		return $this->belongsToMany(Media::class, 'media_mm_tags');
	}

	public function getValidator()
	{

	}
}
