<?php

namespace Larasoft\Admin\Permissions;

use Larasoft\Admin\Base\ModelInterface;
use Larasoft\Admin\Base\SanitisesInputTrait;
use Larasoft\Admin\Users\Role;
use Larasoft\Permissions\Permission as BasePermission;

class Permission extends BasePermission implements ModelInterface
{
	use SanitisesInputTrait;

	public function getValidator()
	{
		return new PermissionValidator;
	}

	public function role()
	{
		return $this->belongsTo(Role::class);
	}
}
