<?php

namespace Larasoft\Admin\Permissions;

use Larasoft\Admin\Base\ModelAdminDecorator;
use Larasoft\Admin\Fields\BelongsToField;
use Larasoft\Admin\Fields\SelectField;
use Larasoft\Admin\Fields\TextField;
use Larasoft\Admin\Reports\Filters\ArrayListingFilter;
use Larasoft\Admin\Users\RoleAdminDecorator;
use Larasoft\Admin\Users\User;
use Larasoft\Permissions\Handler;
use Illuminate\Database\Eloquent\Builder;

class PermissionAdminDecorator extends ModelAdminDecorator
{
	protected $permissions;

	protected $roles;

	public function __construct(Permission $permission, Handler $permissions, RoleAdminDecorator $roles)
	{
		$this->permissions = $permissions;

		$this->roles = $roles;

		parent::__construct($permission);
	}

	public function getLabel($instance)
	{
		$label = $instance->action;

		if ($instance->param) {
			$label .= ' [' . $instance->param . ']';
		}

		return $label;
	}

	protected function getActions()
	{
		$rules = array_keys($this->permissions->dump());

		array_unshift($rules, Permission::WILDCARD);

		return array_combine($rules, $rules);
	}

	public function getFields($instance)
	{
		return [
			new SelectField('action', [
				'options' => $this->getActions(),
				'class' => 'select2'
			]),
			new TextField('param'),
			new BelongsToField($this->roles, $instance->role()),
		];
	}

	public function getColumns($instance)
	{
		return [
			'Action' => $instance->action,
			'Param' => $instance->param ?: '-',
			'Role' => $instance->role ? link_to_route('admin.roles.edit', $this->roles->getLabel($instance->role), [$instance->role->id]) : null,
		];
	}

	public function modifyListingQuery(Builder $query)
	{
		$query->with('role')
			->orderBy('user_id')
			->orderBy('action')
			->orderBy('param');
	}

	public function getListingFilters()
	{
		return [
			new ArrayListingFilter('role', $this->getListOfRoles(), 'role_id'),
			new ArrayListingFilter('action', ['' => 'All'] + $this->getActions(), 'action')
		];
	}

	protected function getListOfRoles()
	{
		return $this->roles->getListingModelsNoLimit()->prepend([
			'id' => null,
			'name' => 'All',
		])->lists('name', 'id');
	}
}
