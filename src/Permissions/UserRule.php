<?php

namespace Larasoft\Admin\Permissions;

use Larasoft\Permissions\Rules\Rule;
use Larasoft\Permissions\UserInterface;

class UserRule extends Rule
{
	public function validFor(UserInterface $user, $instance)
	{
		return $this->isOwnProfile($user, $instance) && $this->checkUserPermissions($user, $instance);
	}

	protected function isOwnProfile(UserInterface $user, $instance)
	{
		return $instance && $user->id === $instance->id;
	}
}
