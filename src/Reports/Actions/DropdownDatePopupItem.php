<?php

namespace Larasoft\Admin\Reports\Actions;

class DropdownDatePopupItem extends FormAction
{
    public function getView()
    {
        return 'admin::report-actions.dropdown-date-popup-item';
    }
}
