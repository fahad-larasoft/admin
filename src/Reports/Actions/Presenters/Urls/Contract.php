<?php

namespace Larasoft\Admin\Reports\Actions\Presenters\Urls;

interface Contract
{
	public function compile($instance);
}
