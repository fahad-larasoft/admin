<?php namespace Larasoft\Admin\Reports;

interface BaseInterface
{
	public function render(array $params = []);
}
