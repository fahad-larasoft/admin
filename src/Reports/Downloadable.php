<?php

namespace Larasoft\Admin\Reports;

interface Downloadable
{
	/**
	 * @param  Larasoft\Admin\Base\Model  $instance
	 * @return array
	 */
	public function getColumnsForCSV($instance);
}
