<?php

namespace Larasoft\Admin\Services\Validators;

use Larasoft\Admin\Services\Validators\Validator;

class EmptyValidator extends Validator
{
	protected function passes($attributes, $rules)
	{
		return true;
	}
}
