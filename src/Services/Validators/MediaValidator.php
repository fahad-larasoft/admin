<?php namespace Larasoft\Admin\Services\Validators;

class MediaValidator extends Validator
{
	protected $storeRules = [
		'filename' => 'required'
	];
}
