<?php

namespace Larasoft\Admin\Users;

use Larasoft\Admin\Services\Validators\UserValidator;

trait Validates
{
	public function getValidator()
	{
		return new UserValidator;
	}
}
