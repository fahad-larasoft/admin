<?php

$permissions->define([

	'admin_login'     => 'Larasoft\Permissions\Rules\GlobalRule',

	'login_as'        => 'Larasoft\Permissions\Rules\GlobalRule',

	'edit_profile'    => 'Larasoft\Admin\Permissions\UserRule',

	'view_anything'   => 'Larasoft\Permissions\Rules\GlobalRule',
	'create_anything' => 'Larasoft\Permissions\Rules\ModelRule',
	'edit_anything'   => 'Larasoft\Permissions\Rules\ModelRule',
	'delete_anything' => 'Larasoft\Permissions\Rules\ModelRule',

	'view_users'   => 'Larasoft\Permissions\Rules\GlobalRule',
	'create_user'  => 'Larasoft\Permissions\Rules\ModelRule',
	'edit_user'    => 'Larasoft\Permissions\Rules\ModelRule',
	'delete_user'  => 'Larasoft\Permissions\Rules\ModelRule',

	'view_media'   => 'Larasoft\Permissions\Rules\GlobalRule',
	'create_media' => 'Larasoft\Permissions\Rules\ModelRule',
	'edit_media'   => 'Larasoft\Permissions\Rules\ModelRule',
	'delete_media' => 'Larasoft\Permissions\Rules\ModelRule',

	'manage_permissions' => 'Larasoft\Permissions\Rules\Rule',

	'assign_roles' => 'Larasoft\Permissions\Rules\ModelRule',
	'edit_user_for_role' => 'Larasoft\Permissions\Rules\ModelRule',
	'delete_user_for_role' => 'Larasoft\Permissions\Rules\ModelRule',

]);
