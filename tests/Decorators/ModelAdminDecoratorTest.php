<?php namespace Larasoft\Admin\Tests\Decorators;

use Mockery;
use Event;
use Larasoft\Admin\Tests\TestCase;
use Larasoft\Admin\Decorators\ModelAdminDecorator;
use Illuminate\Support\Collection;


class ModelAdminDecoratorTest extends TestCase
{
	private $decorator;
	private $model;

	public function testGetModel()
	{
		$this->assertEquals($this->getMockedModel(), $this->getMockedDecorator()->getModel());
	}

	public function testBuildFields()
	{
		$decorator = $this->getMockedDecorator();
		$fieldsData = ['foo' => 'bar'];
		$decorator->shouldReceive('getFields')->once()->andReturn($fieldsData);
		Event::shouldReceive('fire')->once()->with('admin.fields.built', Mockery::type('array'));
		$this->assertEquals($decorator->buildFields(), $fieldsData);
	}

	private function getMockedDecorator($model = null)
	{
		if (is_null($model)) {
			$model = $this->getMockedModel();
		}

		if (empty($this->decorator)) {
			$this->decorator = Mockery::mock('Larasoft\Admin\Decorators\ModelAdminDecorator[]', [$model]);
		}

		return $this->decorator;
	}

	private function getMockedModel()
	{
		if (empty($this->model)) {
			$this->model = Mockery::mock('Larasoft\Admin\Models\Base');
		}

		return $this->model;
	}
}
