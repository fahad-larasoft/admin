<?php namespace Larasoft\Admin\Tests\Fields;

use Larasoft\Admin\Fields\PasswordField;
use TestCase;

class PasswordFieldTest extends TestCase
{
	public function testSuccess()
	{
		$field = new PasswordField(array('name' => 'some_field'));

		$this->assertContains('name="some_field"', $field->getInput());
	}
}
